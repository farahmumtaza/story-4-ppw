from django.shortcuts import render, redirect
from .models import Schedule
from .forms import ScheduleForm
import datetime

# Create your views here.
def schedule_index(request, *args, **kwargs):
    my_form = ScheduleForm(Schedule)
    schedules = Schedule.objects.all()

    context = {
        'form' : my_form,
        'schedules' : schedules
    }
    return render(request, "schedule.html", context)  

def schedule_detail(request, id):
    schedule = Schedule.objects.get(id=id)
    context = {"schedule": schedule}
    return render(request, 'schedule_detail.html', context)

def schedule_create(request):
    my_form = ScheduleForm()

    if request.method == 'POST':
        my_form = ScheduleForm(request.POST)
        if my_form.is_valid():
            Schedule.objects.create(**my_form.cleaned_data)
            return redirect("schedule")

    context = {
        'form' : my_form,
    }
    return render(request, "schedule_create.html", context)  

def remove_schedule(request, id, *args, **kwargs):
    Schedule.objects.filter(id=id).delete()

    return redirect("schedule")  