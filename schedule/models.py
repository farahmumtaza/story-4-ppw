from django.db import models

# Create your models here.

class Schedule(models.Model) :
    kelas = models.CharField(max_length=80)
    dosen = models.CharField(max_length=40)
    jumlahsks = models.IntegerField()
    deskripsi = models.TextField(max_length=100)
    tempat = models.CharField(max_length=40)

    GANJIL = "Ganjil 2019/2020"
    GENAP = "Genap 2019/2020"
    TAHUN_CHOICES = [(GANJIL, 'Ganjil 2019/2020'), (GENAP, 'Genap 2019/2020')]
    tahun = models.CharField(max_length=16, choices=TAHUN_CHOICES, default="GENAP")

    
