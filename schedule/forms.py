from django import forms

class ScheduleForm(forms.Form) :
    kelas = forms.CharField(label="Kelas", max_length=80)
    dosen = forms.CharField(label="Nama Dosen", max_length=40)
    jumlahsks = forms.IntegerField(label="Jumlah SKS")
    deskripsi = forms.CharField(label="Deskripsi", max_length=100)
    tempat = forms.CharField(label="Ruang Kelas", max_length=40)

    GANJIL = "Ganjil 2019/2020"
    GENAP = "Genap 2019/2020"
    TAHUN_CHOICES = [(GANJIL, 'Ganjil 2019/2020'), (GENAP, 'Genap 2019/2020')]
    tahun = forms.ChoiceField(choices=TAHUN_CHOICES, label="Semester")

    

    